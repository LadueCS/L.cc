# L.cc
Problems for the Ladue Computing Contest

# Contributing
You will need a text editor to edit the LaTeX source file. Visual Studio Code with the [LaTeX Workshop](https://github.com/James-Yu/LaTeX-Workshop) extension works great for this. You will also need a LaTeX distribution like TeXLive to generate a PDF from the LaTeX source.
